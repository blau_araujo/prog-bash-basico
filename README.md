# Curso Básico de Programação em Bash

Repositório dos recursos do Curso Básico de Programação em Bash em sua primeira versão, tal como foi lançado em 2019. De lá para cá, muito da forma de apresentação dos conceitos e técnicas foi aprimorado, mas ainda é um material confiável o suficiente para um primeiro contato com a programação do shell padrão do sistema operacional GNU/Linux.

Atualmente, eu recomendo complementar este curso com a [playlist do curso intensivo de programação do Bash](https://youtube.com/playlist?list=PLXoSGejyuQGr53w4IzUzbPCqR4HPOHjAI) e com [o livro "Pequeno Manual do Programador GNU/Bash"](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/).

![](https://blauaraujo.com/wp-content/uploads/2022/05/cafezinho-01.png)

## Links importantes

* [Índice das aulas](aulas/README.md)
* [Playlist completa no Youtube](https://www.youtube.com/watch?v=ZM--I3NJ2jY&list=PLXoSGejyuQGpf4X-NdGjvSlEFZhn2f2H7)
* [Apostila completa (não corrigida) em PDF](https://debxp.org/livros/cbpb-apostila.pdf)
* [Vídeos e textos para aprender o shell do GNU/Linux](https://codeberg.org/blau_araujo/para-aprender-shell)
* [Dúvidas sobre os tópicos do curso (issues)](https://codeberg.org/blau_araujo/prog-bash-basico/issues)
* [Discussões sobre os tópicos de todos os nossos cursos (Gitter)](https://gitter.im/blau_araujo/community)


## Formas de apoio

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com
* [Versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)